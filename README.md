# webnovel-get

Download novels from Webnovel.

## Usage

For help do: `python webnovel-get.py --help`  

Current help:  
```
$ python webnovel-get.py --help
usage: webnovel-get [-h] [-o [OUTPUT]] [-q] [-d] [-n] [-c] [-f {plaintext,html,markdown}] [-V] url

Download novels from Webnovel.

positional arguments:
  url                   The URL of the novel you want to download.

optional arguments:
  -h, --help            show this help message and exit
  -o [OUTPUT], --output [OUTPUT]
                        The file name to save to. By default the program prints it to stdout.
  -q, --quiet           Hides all output. Overrides debug flag.
  -d, --debug           Shows a lot of detail for debug purposes.
  -n, --no-spaces       Removes the spaces between paragraphs.
  -c, --chapter         Download a single chapter instead of a whole book.
  -f {plaintext,html,markdown}, --format {plaintext,html,markdown}
                        Specifies the output format of the downloaded product. Default: plaintext.
  -V, --version         show program's version number and exit
```

You will need to escape these characters in bash: `!()'`

## Notes

If exporting to HTML it is recommended to use the `-s` / `--no-spaces` flag, as line breaks are usually too spaced apart.

## Changelog

### v0.3.0
* Fixed an issue where some paragraphs would be missing and out of order.
* Made it so the `--verbose` flag is triggered by default, and to turn it off the `-q` / `--quiet` flag needs to be issued.
* Added different export options for Plaintext (default), HTML, and Markdown.
* Added a flag `-c` where you can just download a single chapter instead of the whole book.

### v0.2.0

* Added UTF-8 support.

### v0.1.1

* Fixed an issue where chapters wouldn't be downloaded when they had HTML entities in them.

### v0.1

* Initial version.