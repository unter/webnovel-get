# webnovel-get | unter
# https://codeberg.org/unter
# https://codeberg.org/unter/webnovel-get

# TODO:
# - Make an option to download either a whole book or chapter
#/- Add different formats for export, html, markdown, etc
# - Make an option to add an extra space between lines
# - Fix the bug where there's two spaces everywhere randomly (perhaps with a dirty hack like replacing "  " with " ")
# - Instead of "Download complete." display the title and author name
# - Get Title and Author name and somehow display it in finished book
#/- Add non-verbal mode, where no output is shown
#/- Change verbose to default and replace the flag with -q or -s for quiet/silent
# - Add an option to delay for milliseconds or seconds in order to not get flagged as a bot, maybe also a random range
# - Add actual title to HTML title

import argparse
import sys
import re
import urllib
import urllib.request
import html

__version__ = "0.3.0"

parser = argparse.ArgumentParser(prog="webnovel-get", description="Download novels from Webnovel.")
parser.add_argument("url", nargs=1, help="The URL of the novel you want to download.")
parser.add_argument("-o", "--output", nargs="?", type=argparse.FileType("w", encoding="UTF-8"), default=sys.stdout, help="The file name to save to. By default the program prints it to stdout.")
parser.add_argument("-q", "--quiet", action="store_true", help="Hides all output. Overrides debug flag.")
parser.add_argument("-d", "--debug", action="store_true", help="Shows a lot of detail for debug purposes.")
parser.add_argument("-n", "--no-spaces", action="store_true", help="Removes the spaces between paragraphs.")
parser.add_argument("-c", "--chapter", action="store_true", help="Download a single chapter instead of a whole book.")
parser.add_argument("-f", "--format", choices=["plaintext", "html", "markdown"], default="plaintext", help="Specifies the output format of the downloaded product. Default: plaintext.")
parser.add_argument("-V", "--version", action="version", version=f"%(prog)s {__version__}")

args = parser.parse_args()

if args.format == "plaintext":
	chapter_title = "[Chapter {}]\n"
elif args.format == "html":
	html_header = """<!DOCTYPE html>
<html>
\t<head>
\t\t<title>Webnovel</title>
\t</head>
\t<body>\n"""
	html_footer = """\t</body>
</html>"""
	chapter_title = "<h2>Chapter {}</h2>\n"
elif args.format == "markdown":
	chapter_title = "## Chapter {}\n"

def info(text):
	if not args.quiet: print(f"info: {text}")
def debug(text):
	if args.debug and not args.quiet: print(f"debug: {text}")

debug(f"args = {args}")

# This is a WIP
# title_regex = r"<meta property=\"og:title\" content=\"(.*?) - Webnovel"
# description_regex = r""

line_regex = r"<div class=\"dib pr\">\s*<p\s*>\s*(.*?)(?=)\s*<\/p>"
# line_regex = r"<div class=\"dib pr\">\s*<p\s*>\s*(.*?)(?=)\s*\\r\\n" # ALT
line_regex_pirate = r"<div class=\"dib pr\">\s*<p\s*>\s*(.*?)(?=)\s*<pirate>"
chapter_regex = r"(?<=<a href=\")(\/book\/.*?)(?=\" title=)"

replace_html = {
	"<span class=\"entity\"><span>&amp;</span>#34;</span>": "\"",
	"<span class=\"entity\"><span>&amp;</span>#39;</span>": "'",
}

delete_regex = [
	r"<[^<]+?>.*<[^<]+?>\s*", # Remove everything between HMTL tags
	r"\\r\\n\s*",
]

def get_lines(page_html):
	matches = {} # index: match
	results = []
	
	# matches = re.findall(line_regex, page_html)
	matches_iter = re.finditer(line_regex, page_html)
	
	for match in matches_iter:
		debug(f"[{match.start()}] {match.group(1)}")
		matches.update({match.start(): match.group(1)})
	
	matches_iter_pirate = re.finditer(line_regex_pirate, page_html)
	
	for match in matches_iter_pirate:
		debug(f"[{match.start()}] {match.group(1)}")
		matches.update({match.start(): match.group(1)})

	for key in sorted (matches.keys()):
		match_text = matches[key]
		new_match = "Error: something went wrong, and the regex didn't work"
		for entity_from, entity_to in replace_html.items():
			new_match = match_text.replace(entity_from, entity_to)

		for to_delete in delete_regex:
			new_match = re.sub(to_delete, "", new_match)

		new_match = html.unescape(new_match)
		results.append(new_match)

	return results

def download(url):
	request = urllib.request.urlopen(url)
	read_url = request.read()
	return read_url.decode("utf-8")

url = args.url[0]
output = args.output

book = "" if not args.format == "html" else html_header

def format_chapter(chapter_html):
	chapter_text = ""
	chapter_lines = get_lines(chapter_html)
	if args.format == "plaintext":
		for n, line in enumerate(chapter_lines):
			chapter_text += f"{line}\n" if args.no_spaces else f"{line}\n\n"
	elif args.format == "html":
		for n, line in enumerate(chapter_lines):
			chapter_text += f"\t\t<p>{line}</p>\n" if args.no_spaces else f"\t\t<p>{line}</p><br>\n"
	elif args.format == "markdown":
		for n, line in enumerate(chapter_lines):
			chapter_text += f"{line}  \n" if args.no_spaces else f"{line}  \n  \n"
	
	return chapter_text

if args.chapter:
	info(f"Downloading chapter \"{url}\" ...")
	chapter_html = download(url)

	book += format_chapter(chapter_html)
else:
	if not url.endswith("catalog"): url = f"{url}/catalog"

	info(f"Downloading \"{url}\" ...")
	catalog_html = download(url)
	info("Download finished.")
	info("Getting chapter URLs ...")
	chapters = re.findall(chapter_regex, catalog_html)
	# First item of chapters need to go because it is not a real link
	chapters.pop(0)
	info(f"Chapter URLs found ({len(chapters)}).")

	for i, chapter in enumerate(chapters):
		chapter_url = f"https://www.webnovel.com{html.unescape(chapter)}"
		info(f"Downloading Chapter {i+1} ({chapter_url}) ...")
		chapter_html = download(chapter_url)

		book += chapter_title.format(i+1)
		book += format_chapter(chapter_html)
		book += "\n" # Extra newline to seperate chapters

if args.format == "html":
	book += html_footer

output.write(book)

print("Download complete.")